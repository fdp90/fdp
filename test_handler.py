import unittest
from unittest.mock import patch
import pull_request_handler


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.pr_handler = pull_request_handler.Handler('http://localhost:7990', 'user', 'password', 'proj_key', 'repo')

    @patch('pull_request_handler.Handler.post')
    def test_pull_request_create(self, mock_post):
        mock_post.return_value = {'key': 'value'}
        result = self.pr_handler.create(data="open('<filepath>', 'rb')")

        self.assertIsNotNone(result)
        self.assertTrue(isinstance(result, dict))

    @patch('pull_request_handler.Handler._operation')
    @patch('pull_request_handler.Handler.post')
    def test_pull_request_comment(self, mock_post, mock_operation):
        expected = mock_post.return_value = {'key': 'value'}

        result = self.pr_handler.comment(pr_id='31', data="open('<filepath>', 'rb')")

        self.assertIsNotNone(result)
        self.assertTrue(isinstance(result, dict))
        self.assertEqual(result, expected)

        self.assertTrue(mock_post.called)
        self.assertTrue(mock_operation.called)

        # Test with verbose
        self.pr_handler.comment(pr_id='31', data="open('<filepath>', 'rb')", verbose=True)


if __name__ == '__main__':
    unittest.main()

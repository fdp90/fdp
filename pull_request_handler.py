import requests


class Handler(object):
    def __init__(self, hostname, username, auth_token, project, repo, verbose=False):
        self.hostname = hostname
        self.username = username
        self.project = project
        self.repo = repo
        self.auth_token = auth_token
        self.url = '{0}/rest/api/1.0/projects/{1}/repos/{2}/pull-requests'.format(self.hostname, self.project, self.repo)
        self.headers = {'Authorization': 'Bearer {0}'.format(self.auth_token), 'Content-Type': 'application/json'}
        self.verbose = verbose

    def _operation(self, operation, pr_id):
        return '{0}/{1}/{2}'.format(self.url, pr_id, operation)

    def post(self, uri, data, verbose=False):
        response = requests.post(uri, data=data, headers=self.headers).json()

        if verbose or self.verbose:
            print(response)

        return response

    def create(self, data, verbose=False):
        return self.post(self.url, data=data, verbose=verbose)

    def comment(self, pr_id, data, verbose=False):
        return self.post(self._operation('comments', pr_id), data=data, verbose=verbose)

    def add_approver(self, pr_id, data, verbose=False):
        return self.post(self._operation('participants', pr_id), data=data, verbose=verbose)
